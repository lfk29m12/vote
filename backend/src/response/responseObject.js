const httpStatus = require('http-status');

class ResponseObject {
  constructor(options = {}) {
    this.status = options.status || httpStatus.INTERNAL_SERVER_ERROR;
    this.message = options.message || '';
    this.data = options.data || null;
  }
}

module.exports = ResponseObject
