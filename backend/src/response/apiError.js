class ApiError extends Error {
  constructor(message, status, isOperational = true, stack = '') {
    super(message);
    this.name = this.constructor.name;
    this.status = status;
    // 標記這個錯誤是否是操作上的錯誤，也就是說是否由於程式碼邏輯錯誤造成的錯誤。
    this.isOperational = isOperational;

    if (stack) {
      this.stack = stack;
    } else {
      // 使用 Error.captureStackTrace 方法來捕獲建構函式呼叫時的堆疊資訊，並將其設為 ApiError 物件的 stack 屬性。
      Error.captureStackTrace(this, this.constructor);
    }
  }
}

module.exports = ApiError;
