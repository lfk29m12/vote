const httpStatus = require('http-status')
const ResponseObject = require('./responseObject')

/**
 * 標準化回傳規格
 * @param {*} data
 * @param {Number} status
 * @returns {Object} ResponseObject
 */
const formatResponse = (data, status = httpStatus.INTERNAL_SERVER_ERROR) => {
  const options = { status };

  status >= httpStatus.BAD_REQUEST
    ? options.message = data
    : options.data = data;

  const responseObject = new ResponseObject(options);
  return responseObject;
}

module.exports = formatResponse
