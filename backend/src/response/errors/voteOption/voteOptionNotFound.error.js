const { NotFoundError } = require('../base')

class VoteOptionNotFoundError extends NotFoundError {
  constructor(message = '投票選項不存在') {
    super(message)
  }
}

module.exports = VoteOptionNotFoundError
