const ApiError = require("../../apiError");
const httpStatus = require('http-status')

class EmailHasBeenTakenError extends ApiError {
  constructor(message = 'Email已被使用') {
    super(message, httpStatus.BAD_REQUEST)
  }
}

module.exports = EmailHasBeenTakenError
