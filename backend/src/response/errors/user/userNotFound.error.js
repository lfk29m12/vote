const { NotFoundError } = require('../base')

class UserNotFoundError extends NotFoundError {
  constructor(message = '用戶不存在') {
    super(message)
  }
}

module.exports = UserNotFoundError
