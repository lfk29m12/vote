module.exports.EmailHasBeenTakenError = require('./emailHasBeenTaken.error')
module.exports.UserNotFoundError = require('./userNotFound.error')
