const ApiError = require("../../apiError");
const httpStatus = require('http-status')

class UnAuthorizedError extends ApiError {
  constructor(message = '權限不足') {
    super(message, httpStatus.UNAUTHORIZED)
  }
}

module.exports = UnAuthorizedError
