const ApiError = require("../../apiError");
const httpStatus = require('http-status')

class NotFoundError extends ApiError {
  constructor(message = '查無資料') {
    super(message, httpStatus.NOT_FOUND)
  }
}

module.exports = NotFoundError
