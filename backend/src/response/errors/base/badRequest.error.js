const ApiError = require("../../apiError");
const httpStatus = require('http-status')

class BadRequestError extends ApiError {
  constructor(message = '') {
    super(message, httpStatus.BAD_REQUEST)
  }
}

module.exports = BadRequestError
