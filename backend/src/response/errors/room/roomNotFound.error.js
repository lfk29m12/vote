const { NotFoundError } = require('../base')

class RoomNotFoundError extends NotFoundError {
  constructor(message = '房間不存在') {
    super(message)
  }
}

module.exports = RoomNotFoundError
