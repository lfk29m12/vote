const { Op } = require('sequelize');
const { User } = require('../models');
const { REGISTER_BY_DEFAULT } = require('../config/user');
const { Paginate, Query } = require('./plugins');
const { UserRepository } = require('../repositories')
const ServiceBase = require('../base/service.base');
const { UserDTO } = require('../dtos');
const { EmailHasBeenTakenError } = require('../response/errors/user');

class UserService extends ServiceBase {
  constructor() {
    super()
    this.userRepo = new UserRepository()
  }

  async createUser(userBody) {
    if (await this.userRepo.isEmailTaken(userBody.email)) {
      throw new EmailHasBeenTakenError()
    }
    const params = {
      ...userBody,
      register_by: REGISTER_BY_DEFAULT,
      is_email_verified: 1,
    };
    const fields = ['email', 'password', 'name', 'register_by', 'is_email_verified'];
    return User.create(params, { fields });
  }

  async index(reqQuery) {
    const { page, per, search, filter, sort } = reqQuery;
    const paginate = new Paginate(reqQuery);

    const createQuery = () => {
      const queryHandler = new Query();

      // search
      queryHandler.search({
        column: 'email',
        value: search,
        Op: Op.like,
      });

      // filter, 會有多欄位篩選
      queryHandler.filter({
        value: filter,
        handler: ({ getQuery, key, queryValue, filterBoolean, filterDate, filterNormal }) => {
          if (['isEmailVerified'].includes(key)) {
            filterBoolean({ key, queryValue });
          } else if (['createdAt'].includes(key)) {
            filterDate({ key, queryValue });
          }
        },
      });

      // sort, 會有多欄位排序
      queryHandler.sort({ value: sort });

      return queryHandler.getQuery();
    };

    const findResult = await User.findAndCountAll({
      ...createQuery(),
      ...paginate.pagerQuery(),
    });

    return paginate.createResult(findResult);
  }

  async getUser(id) {
    const user = await this.userRepo.getUserById(id);
    return user ? new UserDTO(user) : null
  }

}

module.exports = UserService
