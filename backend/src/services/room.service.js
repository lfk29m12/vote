const { User, VoteOption } = require('../models');
const chartType = require('../config/chart');
const { sequelize } = require('../models');
const logger = require('../config/logger');
const { NAMESPACE_USER_LIST } = require('../config/redis')
const ServiceBase = require('../base/service.base')
const { RoomRepository, UserRepository, VoteOptionRepository } = require('../repositories');
const { UserNotFoundError } = require('../response/errors/user')
const { RoomNotFoundError } = require('../response/errors/room');
const { VoteOptionNotFoundError } = require('../response/errors/voteOption');
const { NotFoundError } = require('../response/errors/base');

class RoomService extends ServiceBase {
  constructor() {
    super()
    this.roomRepo = new RoomRepository()
    this.userRepo = new UserRepository()
    this.voteOptionRepo = new VoteOptionRepository()
  }

  /**
   * 建立房間
   * @param {Object} reqUser
   * @param {Object} data
   * @returns
   */
  async create(reqUser, data) {
    const user = await this.userRepo.getUserById(reqUser.id);
    if (!user) {
      throw new UserNotFoundError()
    }
    // 利用 orm 提供的關聯方法, 來建立該會員的房間
    const room = await user.createRoom({
      title: data.title,
      description: data.description || '',
      outdate_at: data.outdate_at,
    });

    return room;
  }

  async getChartType() {
    return chartType
  }

  /**
   * 抓取房間資訊, 包含該用戶是否投票、投票選項列表
   * @param {Object} Params
   * @property {String} roomId
   * @property {String} userId
   * @returns {Object}
   */
  async getRoom({ roomId, userId }) {
    const room = await this.roomRepo.getRoomById(roomId, {
      include: [
        {
          model: User,
          as: 'owner',
        },
        {
          model: VoteOption,
          as: 'options'
        },
      ]
    })
    if (!room) {
      throw new RoomNotFoundError();
    }
    const users = await this.userRepo.getRoomUsers(room, userId, { joinTableAttributes: ['is_voted'] })
    const is_voted = users.length === 0 ? false : users[0].UserRoomPivot.is_voted
    const roomJSON = room.toJSON()
    roomJSON.is_voted = is_voted
    return roomJSON;
  }

  async _addUsersToRedisRoom({ redisClient, roomId, userIds = [], hasExpired = true }) {
    if (!Array.isArray(userIds)) return
    if (userIds.length === 0) return
    const redisRoomKey = this.roomRepo.createRedisRoomKey(roomId, NAMESPACE_USER_LIST);
    const option = hasExpired ? { EX: process.env.REDIS_EXPIRE_TIME } : null
    await this.roomRepo.redisAddClientToRoom({
      redisClient,
      redisRoomKey,
      userIds: userIds.map(id => `${id}`),
      option
    })
  }

  async _letUsersLeaveRedisRoom({ redisClient, roomId, userIds = [] }) {
    if (!Array.isArray(userIds)) return
    if (userIds.length === 0) return
    const redisRoomKey = this.roomRepo.createRedisRoomKey(roomId, NAMESPACE_USER_LIST);
    await this.roomRepo.redisRemoveClientFromRoom({
      redisClient,
      redisRoomKey,
      userIds: userIds.map(id => `${id}`)
    })
  }

  /**
   * @param {Object} param
   *  @param {Object} socket
   *  @param {Object} roomId
   *  @param {Object} userId
   * @returns {Boolean} 是否加入成功
   */
  async joinRoom({ socket, redisClient, roomId, userId }) {
    const t = await sequelize.transaction();
    const option = { transaction: t };
    try {
      const room = await this.roomRepo.getRoomById(roomId, option);
      const user = await this.userRepo.getUserById(userId, option);

      if (!room || !user) {
        throw new NotFoundError()
      }

      // 紀錄到關聯中介表
      await room.addUser(user, option);

      const userIds = [userId]
      await this._addUsersToRedisRoom({ redisClient, roomId, userIds, hasExpired: false });

      socket.join(roomId);
      await t.commit();
    } catch (err) {
      logger.error(`用戶${userId}加入房間${roomId}失敗！`);
      await t.rollback();
      throw err
    }
  }

  async leaveRoom({ socket, redisClient, roomId, userId }) {
    const t = await sequelize.transaction();
    const option = { transaction: t };
    try {
      const room = await this.roomRepo.getRoomById(roomId, option);
      const user = await this.userRepo.getUserById(userId, option);
      await this._letUsersLeaveRedisRoom({ redisClient, roomId, userIds: [userId] });
      await t.commit();
      return true;
    } catch (err) {
      await t.rollback();
      logger.error(err);
      return false;
    }
  }

  /**
   * 計算該房間線上人數
   * @param {Object} param
   *  @param {String} roomId
   * @returns {Number} 房間總人數
   */
  async countUserInRoom({ redisClient, roomId }) {
    const redisRoomKey = this.roomRepo.createRedisRoomKey(roomId, NAMESPACE_USER_LIST);
    const totalUsers = await this.roomRepo.redisCountRoomUsers({ redisClient, redisRoomKey })
    return totalUsers;
  }

  /**
   * 主機重啟時, 清空redis所有房間的用戶列表
   */
  async initRoomUsers(redisClient) {
    const redisRoomKeys = await this.roomRepo.redisGetRoomKeys({
      redisClient,
      namespace: NAMESPACE_USER_LIST
    })
    if (redisRoomKeys.length === 0) return
    await this.roomRepo.redisClearAllRoom({
      redisClient,
      redisRoomKeys
    })
  }

  async getRoomVotes(roomId) {
    const voteOptions = await this.voteOptionRepo.getVoteOptionByRoomId({
      roomId,
      option: {
        include: {
          model: User,
          as: 'voters',
          attributes: ['id', 'name'],
          through: { attributes: ['reason'] }
        }
      }
    })

    if (!voteOptions) {
      throw new VoteOptionNotFoundError();
    }

    return voteOptions
  }

  // async _existRedisRoom(roomId) {
  //   const redisRoomKey = _createRedisRoomKey(roomId, NAMESPACE_USER_LIST);
  //   const redisClient = redis.getClient();
  //   return await redisClient.exists(redisRoomKey);
  // }
}

module.exports = RoomService

