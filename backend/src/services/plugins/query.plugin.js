const qs = require('qs');
const dayjs = require('dayjs');
const { Op } = require('sequelize');
const { BadRequestError } = require('../../response/errors/base')

class Query {
  constructor(query = null) {
    this.query = query || { where: {} };
  }

  search({ column, value, Op, format }) {
    if (value && typeof value === 'string') {
      const formatter = typeof format === 'function' ? format(value) : `${value}%`;
      const searchQuery = { [column]: { [Op]: formatter } };
      this.query.where = { ...this.query.where, ...searchQuery };
    }
  }

  filter({ value, handler }) {
    if (value) {
      const modifiedFilter = typeof value === 'string' ? [value] : value;
      modifiedFilter.forEach((filterItem) => {
        const parsed = qs.parse(filterItem); // ex: isEmailVerified=0 => { isEmailVerified: '0' }
        Object.keys(parsed).forEach((key) => {
          const queryValue = parsed[key];
          if (typeof handler === 'function')
            handler({
              getQuery: this.getQuery.bind(this),
              setQuery: this.setQuery.bind(this),
              filterBoolean: this.filterBoolean.bind(this),
              filterDate: this.filterDate.bind(this),
              filterNormal: this.filterNormal.bind(this),
              queryValue,
              key,
            });
        });
      });
    }
  }

  filterBoolean({ key, queryValue, setQuery = true }) {
    const booleanValue = queryValue === '1' || queryValue === 'true' ? 1 : 0;
    const filterQuery = { [key]: booleanValue };
    if (setQuery) this.query.where = { ...this.query.where, ...filterQuery };
    return filterQuery;
  }

  filterDate({ key, queryValue, setQuery = true }) {
    const dates = this._getDateRange(queryValue.split('.'));
    if (!dates) {
      throw new BadRequestError('filter date 參數不完全');
    }
    const [startAt, endAt] = dates;
    const filterQuery = {
      [key]: {
        [Op.gte]: `${startAt} 00:00:00`,
        [Op.lte]: `${endAt} 23:59:59`,
      },
    };
    if (setQuery) this.query.where = { ...this.query.where, ...filterQuery };
    return filterQuery;
  }

  filterNormal({ key, queryValue, setQuery = true }) {
    const filterQuery = { [key]: { [Op.like]: `%${queryValue}%` } };
    if (setQuery) this.query.where = { ...this.query.where, ...filterQuery };
    return filterQuery;
  }

  _getDateRange(dates) {
    if (dates.length === 0) return;
    const format = 'YYYY-MM-DD';
    const startAt = dates[0];
    const endAt = dates[1] ? dates[1] : startAt;
    if (!dayjs(startAt, format, true).isValid()) return;
    if (!dayjs(endAt, format, true).isValid()) return;

    return [startAt, endAt];
  }

  sort({ value }) {
    if (value) {
      const sortColumns = [];
      const sortItems = typeof value === 'string' ? [value] : value;
      sortItems.forEach((sortItem) => {
        const parsed = qs.parse(sortItem); // ex: isEmailVerified=0 => { isEmailVerified: '0' }
        Object.keys(parsed).forEach((key) => {
          const queryValue = parsed[key];
          sortColumns.push([key, queryValue.toUpperCase()]);
        });
      });
      this.query.order = sortColumns;
    }
  }

  getQuery() {
    return this.query;
  }

  setQuery(query) {
    this.query = query;
    return this.query;
  }
}

module.exports = Query;
