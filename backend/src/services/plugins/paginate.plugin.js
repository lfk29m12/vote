class Paginate {
  constructor(query) {
    this.query = query;
    this.per = +this.query.per;
    this.page = +this.query.page;
  }

  pagerQuery() {
    return {
      limit: this.per,
      offset: (this.page - 1) * this.per,
    };
  }

  createResult({ rows, count }) {
    const pages = Math.ceil(count / this.per);
    return {
      data: rows,
      pager: {
        total: count,
        count: Math.min(count - (this.page - 1) * this.per, this.per),
        per: this.per,
        page: this.page,
        pages,
        has_next: pages > 1 && pages > this.page,
        has_previous: pages > 1 && this.page > 1,
      },
    };
  }
}

module.exports = Paginate;
