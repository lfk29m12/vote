const TokenService = require('./token.service');
const UserService = require('./user.service');
const { TOKEN_TYPE_REFRESH } = require('../config/token');
const ServiceBase = require('../base/service.base')
const { TokenRepository, UserRepository } = require('../repositories');
const { NotFoundError, UnAuthorizedError } = require('../response/errors/base');

class AuthService extends ServiceBase {
  constructor() {
    super()
    this.tokenService = new TokenService()
    this.userService = new UserService()
    this.userRepo = new UserRepository()
    this.tokenRepo = new TokenRepository()
  }

  async refreshAuth(refreshToken) {
    try {
      const token = await this.tokenService.verifyToken(refreshToken, TOKEN_TYPE_REFRESH);
      const user = await this.userRepo.getUserById(token.user_id);
      if (!user) {
        throw new Error()
      }
      await token.destroy();
      return this.tokenService.generateAuthTokens(user);
    } catch (error) {
      throw new UnAuthorizedError(error.message)
    }
  }

  /**
   * Login with username and password
   * @param {string} email
   * @param {string} password
   * @returns {Promise<User>}
   */
  async loginUserWithEmailAndPassword(email, password) {
    const user = await this.userRepo.getUserByEmail(email, 'withPassword');
    if (!user || !(await user.isPasswordMatch(password))) {
      throw new UnAuthorizedError('Incorrect email or password')
    }
    return user;
  }

  /**
   * Logout
   * @param {string} refreshToken
   * @returns {Promise}
   */
  async logout(refreshToken) {
    const token = await this.tokenRepo.findTokenWithoutUserId({
      token: refreshToken,
      type: TOKEN_TYPE_REFRESH,
      blacklisted: false,
    })
    if (!token) {
      throw new NotFoundError()
    }
    await token.destroy();
  }

}

module.exports = AuthService

