const jwt = require('jsonwebtoken');
const dayjs = require('dayjs');
const config = require('../config/config');
const { Token } = require('../models');
const { TOKEN_TYPE_ACCESS, TOKEN_TYPE_REFRESH } = require('../config/token');
const { TokenRepository } = require('../repositories');
const ServiceBase = require('../base/service.base')
const { NotFoundError } = require('../response/errors/base');

class TokenService extends ServiceBase {
  constructor() {
    super()
    this.tokenRepo = new TokenRepository()
  }

  /**
   * Save a token
   * @param {string} token
   * @param {ObjectId} userId
   * @param {Dayjs} expires
   * @param {string} type
   * @param {boolean} [blacklisted]
   * @returns {Promise<Token>}
   */
  async saveToken(token, userId, expires, type, blacklisted = false) {
    return await this.tokenRepo.createToken({
      token,
      userId,
      expires,
      type,
      blacklisted,
    })
  }

  /**
   * Verify token and return token doc (or throw an error if it is not valid)
   * @param {String} token
   * @param {String} type
   * @returns {Object<Token>}
   */
  async verifyToken(token, type) {
    const payload = jwt.verify(token, config.jwt.secret);
    const tokenModel = await this.tokenRepo.findToken({
      token, type, userId: payload.sub, blacklisted: false
    })
    if (!tokenModel) {
      throw new NotFoundError('Token not found')
    }
    return tokenModel;
  }

  /**
   * Generate token
   * @param {ObjectId} userId
   * @param {Dayjs} expires
   * @param {string} type
   * @param {string} [secret]
   * @returns {string}
   */
  generateToken(userId, expires, type, secret = config.jwt.secret) {
    const payload = {
      sub: userId,
      iat: dayjs().unix(),
      exp: expires.unix(),
      type,
    };
    return jwt.sign(payload, secret);
  }

  /**
   * Generate auth tokens
   * @param {User} user
   * @returns {Promise<Object>}
   */
  async generateAuthTokens(user) {
    const accessTokenExpires = dayjs().add(config.jwt.accessExpirationMinutes, 'minute');
    const accessToken = this.generateToken(user.id, accessTokenExpires, TOKEN_TYPE_ACCESS);

    const refreshTokenExpires = dayjs().add(config.jwt.refreshExpirationDays, 'day');
    const refreshToken = this.generateToken(user.id, refreshTokenExpires, TOKEN_TYPE_REFRESH);
    // 只存 refresh token
    await this.saveToken(refreshToken, user.id, refreshTokenExpires, TOKEN_TYPE_REFRESH);

    return {
      access: {
        token: accessToken,
        expires: accessTokenExpires.toDate(),
      },
      refresh: {
        token: refreshToken,
        expires: refreshTokenExpires.toDate(),
      },
    };
  }

}

module.exports = TokenService
