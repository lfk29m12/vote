const Validation = require('./validation');

class UserValidation extends Validation {
  createUser() {
    return {
      body: this.Joi.object().keys({
        email: this.Joi.string().required().email(),
        password: this.Joi.string().required().custom(this.custom.password),
        name: this.Joi.string().required(),
        role: this.Joi.string().required().valid('user', 'admin'),
      }),
    };
  }

  getUsers() {
    return {
      query: this.Joi.object().keys({
        name: this.Joi.string(),
        role: this.Joi.string(),
        sortBy: this.Joi.string(),
        limit: this.Joi.number().integer(),
        page: this.Joi.number().integer(),
      }),
    };
  }

  getUser() {
    return {
      params: this.Joi.object().keys({
        userId: this.Joi.string(),
      }),
    };
  }

  updateUser() {
    return {
      params: this.Joi.object().keys({
        userId: this.Joi.required(),
      }),
      body: this.Joi.object()
        .keys({
          email: this.Joi.string().email(),
          password: this.Joi.string().custom(password),
          name: this.Joi.string(),
        })
        .min(1),
    };
  }

  deleteUser() {
    return {
      params: this.Joi.object().keys({
        userId: this.Joi.string(),
      }),
    };
  }
}

module.exports = new UserValidation();
