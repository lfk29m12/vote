const Validation = require('./validation');

class AuthValidation extends Validation {
  register() {
    return {
      body: this.Joi.object().keys({
        email: this.Joi.string().required().email(),
        password: this.Joi.string().required().custom(this.custom.password),
        name: this.Joi.string().required(),
      }),
    };
  }

  login() {
    return {
      body: this.Joi.object().keys({
        email: this.Joi.string().required(),
        password: this.Joi.string().required(),
      }),
    };
  }

  logout() {
    return {
      body: this.Joi.object().keys({
        refreshToken: this.Joi.string().required(),
      }),
    };
  }

  refreshTokens() {
    return {
      body: this.Joi.object().keys({
        refreshToken: this.Joi.string().required(),
      }),
    };
  }

  forgotPassword() {
    return {
      body: this.Joi.object().keys({
        email: this.Joi.string().email().required(),
      }),
    };
  }

  resetPassword() {
    return {
      query: this.Joi.object().keys({
        token: this.Joi.string().required(),
      }),
      body: this.Joi.object().keys({
        password: this.Joi.string().required().custom(password),
      }),
    };
  }

  verifyEmail() {
    return {
      query: this.Joi.object().keys({
        token: this.Joi.string().required(),
      }),
    };
  }
}

module.exports = new AuthValidation();
