const Validation = require('./validation');
const { basicDateFormat } = require('../config/date');

class RoomValidation extends Validation {
  createRoom() {
    return {
      body: this.Joi.object().keys({
        title: this.Joi.string().required(),
        description: this.Joi.string(),
        outdate_at: this.Joi.date().greater(new Date()).format(basicDateFormat).required(),
      }),
    };
  }
}

module.exports = new RoomValidation();
