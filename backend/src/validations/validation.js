const Joi = require('joi').extend(require('@joi/date'));

class Validation {
  constructor() {
    this.Joi = Joi;
    this.custom = {
      password: (value, helpers) => {
        if (value.length < 4) {
          return helpers.message('password must be at least 4 characters');
        }
        // if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
        //   return helpers.message('password must contain at least 1 letter and 1 number');
        // }
        return value;
      },
    };
  }

  index() {
    const regexPattern = /^(\w+)=(.*)$/;
    return {
      query: Joi.object().keys({
        page: Joi.number(),
        per: Joi.number(),
        search: Joi.string(),
        filter: Joi.alternatives().try(
          Joi.array().items(Joi.string().pattern(regexPattern)),
          Joi.string().pattern(regexPattern)
        ),
        sort: Joi.alternatives().try(
          Joi.array().items(Joi.string().pattern(regexPattern)),
          Joi.string().pattern(regexPattern)
        ),
      }),
    };
  }
}

module.exports = Validation;
