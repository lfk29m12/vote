/** @type {import('sequelize-cli').Migration} */

const { User, Room, UserRoomPivot } = require('../models');
const { Op } = require('sequelize');
const { userRoomPivotFactory } = require('../factories')

const { createPivotData } = userRoomPivotFactory

async function createPivots() {
  const user2 = await User.findOne({ where: { name: 'tester2' } });
  const user3 = await User.findOne({ where: { name: 'tester3' } });
  const room1 = await Room.findOne({ where: { title: '房間1' } })
  return [
    createPivotData({
      user_id: user2.id,
      room_id: room1.id
    }),
    createPivotData({
      user_id: user3.id,
      room_id: room1.id
    }),
  ]
}

module.exports = {
  async up(queryInterface, Sequelize) {
    const pivots = await createPivots()
    const userRoomPivots = await queryInterface.bulkInsert(
      'user_room_pivots',
      pivots,
      {}
    );
  },

  async down(queryInterface, Sequelize) {

    const user2 = await User.findOne({
      where: { name: 'tester2' },
    });

    const user3 = await User.findOne({
      where: { name: 'tester3' },
    });

    const room1 = await Room.findOne({
      where: { title: '房間1' }
    })

    await UserRoomPivot.destroy({
      where: {
        [Op.or]: [
          {
            user_id: user2.id,
            room_id: room1.id
          },
          {
            user_id: user3.id,
            room_id: room1.id
          }
        ]
      },
    })
  },
};
