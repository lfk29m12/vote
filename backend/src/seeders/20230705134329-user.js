/** @type {import('sequelize-cli').Migration} */

const { Op } = require('sequelize');
const { userFactory } = require('../factories')

const { createUser1Data, createUser2Data, createUser3Data } = userFactory
const users = [
  createUser1Data(),
  createUser2Data(),
  createUser3Data(),
]

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'users',
      users,
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('users', { [Op.or]: users.map(user => ({ name: user.name })) }, {});
  },
};
