/** @type {import('sequelize-cli').Migration} */

const { User } = require('../models');
const { Op } = require('sequelize');
const { roomFactory } = require('../factories')

const { createRoom1Data } = roomFactory

const userQueryCondition = { where: { name: 'tester1' } };

async function createRooms() {
  const user = await User.findOne(userQueryCondition);
  const userId = user.id
  const rooms = [
    createRoom1Data({ owner_id: userId })
  ]
  return rooms
}

async function getRooms() {
  const user = await User.findOne(userQueryCondition);
  return await user.getRooms()
}

module.exports = {
  async up(queryInterface, Sequelize) {
    const rooms = await createRooms()
    await queryInterface.bulkInsert(
      'rooms',
      rooms,
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    const rooms = await getRooms()
    await queryInterface.bulkDelete(
      'rooms',
      {
        [Op.or]: rooms.map(room => ({ id: room.id }))
      },
      {}
    );
  },
};
