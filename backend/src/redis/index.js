const redis = require('redis');
const logger = require('../config/logger');
const RedisClient = require('./_client')

class Redis {
  async createClient() {
    const client = redis.createClient({
      socket: {
        host: 'redis',
        port: 6379,
      },
    });

    if (client.connect) {
      await client.connect();
    }

    client.on('error', (error) => logger.error(`redis client error: ${error}`));

    return new RedisClient(client)
  }

  async getKeys(client, { pattern = "*", count = 100 }) {
    const results = [];
    const iteratorParams = {
      MATCH: pattern,
      COUNT: count
    }
    for await (const key of client.scanIterator(iteratorParams)) {
      results.push(key);
    }
    return results;
  }

}


module.exports = new Redis();
