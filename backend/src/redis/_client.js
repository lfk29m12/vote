class RedisClient {
  constructor(client) {
    this.isEnv = process.env.NODE_ENV === 'test'
    this.client = client
  }

  async SADD(key, value, option) {
    if (this.isEnv) {
      return await new Promise(resolve => {
        this.client.SADD(key, value, (err, result) => {
          resolve(result)
        });
      })
    }
    return await this.client.SADD(key, value, option)
  }

  async SCARD(key) {
    if (this.isEnv) {
      return await new Promise(resolve => {
        this.client.SCARD(key, (err, result) => {
          resolve(result)
        });
      })
    }
    return await this.client.SCARD(key)
  }

  async SREM(key, value) {
    if (this.isEnv) {
      return await new Promise(resolve => {
        this.client.SREM(key, value, (err, result) => {
          resolve(result)
        });
      })
    }
    return await this.client.SREM(key, value)
  }

  async DEL(key) {
    if (this.isEnv) {
      return await new Promise(resolve => {
        this.client.DEL(key, (err, result) => {
          resolve(result)
        });
      })
    }
    return await this.client.DEL(key)
  }

  scanIterator(iteratorParams) {
    return this.client.scanIterator(iteratorParams)
  }
}

module.exports = RedisClient
