const DTOBase = require('../base/dto.base')

class UserDTO extends DTOBase {
  constructor(doc) {
    super(doc)
    this.id = doc.id
    this.name = doc.name
    this.email = doc.email
    this.role = doc.role
    this.is_email_verified = doc.is_email_verified
    this.register_by = doc.register_by
  }
}

module.exports = UserDTO
