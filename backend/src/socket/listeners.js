const { JOIN_ROOM, JOIN_ROOM_ERROR, UPDATE_TOTAL_USER_IN_ROOM } = require('../config/socket');
const { roomHandler } = require('./handlers')
const SocketHelper = require('./helper');
const { errorConverterKernel, errorHandlerKernel } = require('../middlewares/error')

module.exports = async (io, socket) => {
  const redisClient = global.redisClient
  const socketHelper = new SocketHelper({ io, socket });

  /**
   * 捕捉handler丟出的error, 並根據給定的事件傳給client
   * @param {Function} fn
   * @param {Function} errorCb
   * @returns
   */
  const listenerWrap = ({ fn, errorCb }) => async (params) => {
    try {
      await fn(params)
    } catch (err) {
      if (typeof errorCb === 'function') {
        const error = errorHandlerKernel(errorConverterKernel(err))
        errorCb(error)
      }
    }
  }

  // 加入房間
  socket.on(
    JOIN_ROOM,
    listenerWrap({
      fn: roomHandler.onJoinRoom({ io, socket, redisClient }),
      errorCb: (error) => {
        // 向 client 發送自訂錯誤事件
        socketHelper.toSelf({ event: JOIN_ROOM_ERROR, data: error })
      }
    })
  );

  // 可以通过监听disconnecting事件来获取 Socket 所在的房间
  socket.on(
    'disconnecting',
    listenerWrap(
      roomHandler.onDisconnecting({ io, socket, redisClient })
    )
  );

  socket.on(
    'disconnect',
    listenerWrap(() => { })
  );

  // 監聽客戶端發送的訊息
  // socket.on('emit', ({ roomId, msg }) => {
  //   global.emitter.to(roomId).emit('broadcast', msg);
  // });
};
