const { RoomService } = require('../../services');
const logger = require('../../config/logger');
const { UPDATE_TOTAL_USER_IN_ROOM } = require('../../config/socket');
const SocketHelper = require('../helper');
const formatResponse = require('../../response/formatResponse')
const httpStatus = require('http-status')

module.exports.onJoinRoom = ({ io, socket, redisClient }) => async ({ roomId }) => {
  const socketHelper = new SocketHelper({ io, socket });
  const userId = socketHelper.getUserId();
  const roomService = new RoomService()
  logger.info(`用戶${userId}嘗試加入房間${roomId}`);

  await roomService.joinRoom({ redisClient, socket, roomId, userId });
  const totalUsers = await roomService.countUserInRoom({ redisClient, roomId });
  logger.info(`房間在線人數: ${totalUsers}`);

  // 更新總人數
  socketHelper.toRoomUsers({
    roomId,
    event: UPDATE_TOTAL_USER_IN_ROOM,
    data: formatResponse(totalUsers, httpStatus.OK)
  });
}

module.exports.onDisconnecting = ({ io, socket, redisClient }) => async () => {
  const socketHelper = new SocketHelper({ io, socket });
  const userId = socketHelper.getUserId();
  const roomService = new RoomService()

  logger.info(`用戶${userId}斷線`);

  socket.rooms.forEach(async (roomId) => {
    if (roomId !== socket.id) {
      const isSuccess = await roomService.leaveRoom({ socket, redisClient, roomId, userId });

      if (isSuccess) {
        const totalUsers = await roomService.countUserInRoom({ redisClient, roomId });
        logger.info(`房間在線人數: ${totalUsers}`);

        // 更新總人數給其他用戶
        socketHelper.toOtherRoomUsers({
          roomId,
          event: UPDATE_TOTAL_USER_IN_ROOM,
          data: formatResponse(totalUsers, httpStatus.OK)
        });
      }
    }
  });
}
