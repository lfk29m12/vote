class SocketHelper {
  constructor({ io, socket }) {
    this.io = io;
    this.socket = socket;
  }

  getUserId() {
    return this.socket.request.user.id
  }

  _getUserIdKernel(socket) {
    return socket.handshake.auth.userId;
  }

  async getRoomSockets(roomId) {
    return await this.io.in(roomId).fetchSockets();
  }

  async getRoomUserIds(roomId) {
    const sockets = await this.getRoomSockets(roomId)
    return [...new Set(sockets.map(socket => this._getUserIdKernel(socket)))]
  }

  /**
 * 發給房間除了自己以外的其他用戶
 * @param {*} param
 *  @param {String} roomId
 *  @param {String} event - 事件名稱
 *  @param {Any} data - 資料
 */
  toOtherRoomUsers({ roomId, event, data }) {
    this.socket.to(roomId).emit(event, data);
  }

  /**
   * 發給房間所有用戶
   * @param {Object} param0
   * @param {String} param0.roomId
   * @param {String} param0.event - 事件名稱
   * @param {Any} param0.data - 資料
   */
  toRoomUsers({ roomId, event, data }) {
    this.io.in(roomId).emit(event, data);
  }

  /**
   * 發給當前用戶
   * @param {Object} param0
   * @param {String} param0.event - 事件名稱
   * @param {Any} param0.data - 資料
   */
  toSelf({ event, data }) {
    this.io.to(this.socket.id).emit(event, data)
  }
}

module.exports = SocketHelper;
