const { Server } = require('socket.io');
const logger = require('../config/logger');
const listeners = require('./listeners');

class Socket {
  async init(server) {
    this.io = new Server(server, {
      cors: {
        origin: '*',
      },
    });

    // 當有客戶端連線進來才執行
    this.io.on('connection', (socket) => {
      console.log('用戶已連線', socket.handshake.headers.authorization);
      listeners(this.io, socket);
    });

    return this.io
  }
}

module.exports = new Socket();
