const formatResponse = require('../response/formatResponse')

class ControllerBase {
  formatResponse(data, status = httpStatus.INTERNAL_SERVER_ERROR) {
    return formatResponse(data, status)
  }
}

module.exports = ControllerBase
