class DTOBase {
  constructor(doc, option = { date: true }) {
    const hasDate = option.date
    if (hasDate) {
      if (doc.create_at) this.create_at = doc.create_at
      if (doc.updated_at) this.updated_at = doc.updated_at
    }
  }
}

module.exports = DTOBase
