const { Router } = require('express')

class RouteBase {
  constructor() {
    this.controller = null
    this.router = Router()
    this.initial();
  }

  initial() {
    this.registerRoute();
  }

  // 子類別自行定義
  registerRoute() { }

  getRouter() {
    return this.router
  }

  responseHandler(method, controller = this.controller) {
    return (req, res, next) => {
      method.call(controller, req, res, next)
        // controller return formatResponse => 由此處接手處理
        .then(obj => {
          res.status(obj.status).json(obj)
        })

        // 只要 controller/service/repository 中丟出錯誤就會被此處catch, 並轉成ResponseObject
        // next() 有帶入錯誤訊息，Express 就會視為錯誤 => 由全域的 defaultException middleware 接手處理
        // .catch(err => next(controller.formatResponse(err.message, err.status || httpStatus.INTERNAL_SERVER_ERROR)))

        // next() 有帶入錯誤訊息，Express 就會視為錯誤
        // 將錯誤丟給 errorConverter => errorHandler middleware 處理
        .catch(err => {
          next(err)
        })
    }
  }
}

module.exports = RouteBase
