module.exports.userFactory = require('./user.factory')
module.exports.roomFactory = require('./room.factory')
module.exports.userRoomPivotFactory = require('./userRoomPivot.factory')
module.exports.voteOptionFactory = require('./voteOption.factory')
