const { Room } = require('../models')
const { v4: uuidv4 } = require('uuid');
const dayjs = require('dayjs');

const _createData = (params = {}) => {
  return {
    // title: params.title,
    // description: params.description,
    // owner_id: params.owner_id,
    uuid: uuidv4(),
    chart_type: 1,
    outdate_at: dayjs().add(1, 'day').toDate(),
    // created_at: new Date(),
    // updated_at: new Date(),
    ...params,
  };
}

const createRoom1Data = (params) => _createData({
  title: '房間1',
  description: '中午吃啥',
  ...params,
})

/**
 * @param {Object} param0
 * @param {User} param0.user
 * @param {Object} param0.data - room object
 * @returns {Room}
 */
async function insertRoom({ user, data }) {
  return await user.createOwnRoom(data)
}


module.exports = {
  insertRoom,
  createRoom1Data,
}
