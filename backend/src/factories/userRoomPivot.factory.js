const { Room, UserRoomPivot } = require('../models')

const createPivotData = (params) => {
  return {
    // room_id: params.room_id,
    // user_id: params.user_id,
    is_voted: 0,
    // created_at: new Date(),
    // updated_at: new Date(),
    ...params,
  }
}

/**
 *
 * @param {Object} data - userRoomPivot object
 * @returns {UserRoomPivot}
 */
async function insertPivot(data) {
  return await UserRoomPivot.create(data)
}


module.exports = {
  insertPivot,
  createPivotData,
}
