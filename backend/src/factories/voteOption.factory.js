const { VoteOption } = require('../models')

const _createData = (params = {}) => {
  return {
    // order: params.order,
    // title: params.title,
    // room_id: params.room_id,
    // created_at: new Date(),
    // updated_at: new Date(),
    ...params,
  };
}

const createVoteOption1Data = (params) => _createData({
  title: 'pizza hot',
  order: 0,
  ...params,
})

const createVoteOption2Data = (params) => _createData({
  title: '麥當勞',
  order: 1,
  ...params,
})

module.exports = {
  createVoteOption1Data,
  createVoteOption2Data
}
