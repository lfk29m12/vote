const { User, Token } = require('../models')
const { TokenService } = require('../services')
const bcrypt = require('bcryptjs');

const _createData = (params = {}) => {
  return {
    // name: 'tester1',
    // email: 'tester1@gmail.com',
    password: bcrypt.hashSync('1234', bcrypt.genSaltSync(8), null),
    role: 'user',
    is_email_verified: 1,
    register_by: 0,
    // created_at: new Date(),
    // updated_at: new Date(),
    ...params,
  };
}

const createUser1Data = (params) => _createData({
  name: 'tester1',
  email: 'tester1@gmail.com',
  ...params,
})

const createUser2Data = (params) => _createData({
  name: 'tester2',
  email: 'tester2@gmail.com',
  ...params,
})

const createUser3Data = (params) => _createData({
  name: 'tester3',
  email: 'tester3@gmail.com',
  ...params,
})

async function insertUsers({ data, withToken = false }) {
  const users = await User.bulkCreate(data)
  if (withToken) {
    const list = []
    for (let user of users) {
      const tokenModel = await new TokenService().generateAuthTokens(user);
      const { token } = tokenModel.access
      list.push({ user, token })
    }
    return list
  }
  return users
}


module.exports = {
  insertUsers,
  createUser1Data,
  createUser2Data,
  createUser3Data,
}
