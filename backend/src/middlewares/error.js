const httpStatus = require('http-status');
const config = require('../config/config');
const logger = require('../config/logger');
const ApiError = require('../response/apiError');
const ResponseObject = require('../response/responseObject')

const errorConverterKernel = (err) => {
  let error = err;
  if (!(error instanceof ApiError)) {
    const status = error.status || httpStatus.INTERNAL_SERVER_ERROR;
    const message = error.message || httpStatus[status];
    error = new ApiError(status, message, false, err.stack);
  }
  return error
}

const errorHandlerKernel = (err) => {
  let { status, message } = err;
  if (config.env === 'production' && !err.isOperational) {
    status = httpStatus.INTERNAL_SERVER_ERROR;
    message = httpStatus[httpStatus.INTERNAL_SERVER_ERROR];
  }

  // 定義回傳訊息格式
  const error = new ResponseObject({
    status,
    message,
  })

  if (config.env === 'development') {
    // 只有開發模式才有stack屬性, 方便除錯
    error.stack = err.stack
    logger.error(err);
  }

  return error
}

/**
 * 將所有錯誤轉成 ApiError
 * @param {*} err
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const errorConverter = (err, req, res, next) => {
  const error = errorConverterKernel(err)
  next(error);
};

/**
 * 將錯誤轉成物件, 並定義錯誤格式, 返回給前端
 * @param {*} err
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const errorHandler = (err, req, res, next) => {
  let { status, message } = err;
  const error = errorHandlerKernel(err)
  // 用來給 morgan 顯示錯誤訊息
  res.locals.errorMessage = message;
  res.status(status).send(error);
};

module.exports = {
  errorConverterKernel,
  errorHandlerKernel,
  errorConverter,
  errorHandler,
};
