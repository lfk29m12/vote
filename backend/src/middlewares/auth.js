const passport = require('passport');
const { UnAuthorizedError } = require('../response/errors/base')
const { roleRights } = require('../config/roles');

/**
 * 驗證成功的請求會帶上 user model
 * @param {*} req
 * @param {*} resolve
 * @param {*} reject
 * @param {*} requiredRights - 該api需要的權限
 * @returns {Function}
 *  @param {Object} err 如果例外發生, err 會被設定
 *  @param {Object} user 來自 JwtStrategy 驗證後, 丟給 done() 的 user 資訊, 如果認證失敗，user 會被設成 false
 *  @param {Object} info info 則可以拿到 strategy 中 verify callback 所提供的更多訊息
 */
const verifyCallback = (req, resolve, reject, requiredRights) => async (err, user, info) => {
  if (err || info || !user) {
    return reject(new UnAuthorizedError());
  }

  req.user = user;

  // 暫時不規劃權限檢查
  // if (requiredRights.length) {
  //   const userRights = roleRights.get(user.role);
  //   const hasRequiredRights = requiredRights.every((requiredRight) => userRights.includes(requiredRight));
  //   if (!hasRequiredRights) {
  //     return reject(new ApiError(httpStatus.FORBIDDEN, 'Forbidden'));
  //   }
  // }

  resolve();
};

/**
 * 返回 middleware, 用來驗證請求
 * @param  {...any} requiredRights
 * @returns
 */
const auth =
  (option = {}) =>
    async (req, res, next) => {
      return new Promise((resolve, reject) => {
        const { requiredRights = [] } = option
        /**
         * passport.authenticate() 返回一個 middleware
         * 參考: https://pjchender.dev/npm/npm-passport/#%E5%AE%A2%E5%88%B6%E5%8C%96-callback%E5%B8%B8%E7%94%A8
         */
        passport.authenticate('jwt', { session: false }, verifyCallback(req, resolve, reject, requiredRights))(req, res, next);
      })
        .then(() => next())
        .catch((error) => {
          const { onError } = option
          if (typeof onError === 'function') {
            onError({ error, next, req, res })
          } else {
            next(error)
          }
        });
    };

module.exports = auth;
