/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const { INTEGER, STRING, DATE, BOOLEAN } = Sequelize;
    await queryInterface.createTable('tokens', {
      id: {
        type: INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      token: {
        type: STRING,
        allowNull: false,
      },
      user_id: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'users',
          },
          key: 'id',
        },
        onDelete: 'CASCADE',
      },
      type: {
        type: INTEGER(2).UNSIGNED,
        allowNull: false,
        comment: 'Token類別 (0: access, 1: refresh)',
      },
      expires: {
        type: DATE,
        allowNull: false,
      },
      blacklisted: {
        type: BOOLEAN,
        allowNull: false,
      },
      created_at: {
        type: DATE,
        allowNull: false,
      },
      updated_at: {
        type: DATE,
        allowNull: false,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('tokens');
  },
};
