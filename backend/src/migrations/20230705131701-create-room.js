/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const { INTEGER, STRING, DATE, BOOLEAN, UUID, UUIDV4 } = Sequelize;
    await queryInterface.createTable('rooms', {
      id: {
        type: INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      uuid: {
        type: UUID,
        defaultValue: UUIDV4,
        allowNull: false,
      },
      owner_id: {
        type: INTEGER,
        references: {
          model: {
            tableName: 'users',
          },
          key: 'id',
        },
        onDelete: 'CASCADE',
      },
      title: {
        type: STRING,
        allowNull: false,
      },
      description: {
        type: STRING,
      },
      outdate_at: {
        type: DATE,
        allowNull: false,
      },
      chart_type: {
        type: INTEGER,
        comment: '圖表類型',
        defaultValue: 0, // 查看 config/chart.js
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DATE,
      },
      updated_at: {
        allowNull: false,
        type: DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('rooms');
  },
};
