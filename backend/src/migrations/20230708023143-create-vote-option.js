/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const { INTEGER, STRING, DATE } = Sequelize;
    await queryInterface.createTable('vote_options', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: INTEGER,
      },
      title: {
        type: STRING,
        require: true,
      },
      order: {
        type: INTEGER,
        defaultValue: 0,
        require: true,
      },
      room_id: {
        type: INTEGER,
        references: {
          model: 'rooms',
          key: 'id',
        },
      },
      created_at: {
        allowNull: false,
        type: DATE,
      },
      updated_at: {
        allowNull: false,
        type: DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('vote_options');
  },
};
