/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const { INTEGER, BOOLEAN, DATE } = Sequelize;
    await queryInterface.createTable('user_room_pivots', {
      id: {
        type: INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      room_id: {
        type: INTEGER,
        references: {
          model: 'rooms',
          key: 'id',
        },
      },
      user_id: {
        type: INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      is_voted: {
        type: BOOLEAN,
        defaultValue: false,
      },
      created_at: {
        allowNull: false,
        type: DATE,
      },
      updated_at: {
        allowNull: false,
        type: DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('user_room_pivots');
  },
};
