const RouteBase = require('../base/route.base')
const ApiRoute = require('./api.route')

class AppRoute extends RouteBase {

  constructor() {
    super();
  }

  initial() {
    this.apiRoute = new ApiRoute()
    super.initial();
  }

  registerRoute() {
    this.router.use('/api', this.apiRoute.router);
  }

};

module.exports = AppRoute
