const { UserController } = require('../../controllers')
const RouteBase = require('../../base/route.base')
const { auth, validate } = require('../../middlewares');
const { userValidation } = require('../../validations');

class UserRoute extends RouteBase {

  constructor() {
    super();
  }

  initial() {
    this.controller = new UserController();
    super.initial();
  }

  registerRoute() {
    this.router.route('/index').get(
      validate(userValidation.index()),
      this.responseHandler(this.controller.index)
    );

    this.router.route('/profile').get(
      auth(),
      this.responseHandler(this.controller.readProfile)
    );

    this.router.route('/:userId').get(
      auth(),
      validate(userValidation.getUser()),
      this.responseHandler(this.controller.getUser)
    );
  }
}

module.exports = UserRoute
