const { AuthController } = require('../../controllers')
const RouteBase = require('../../base/route.base')
const { auth, validate } = require('../../middlewares');
const { authValidation } = require('../../validations');

class AuthRoute extends RouteBase {

  constructor() {
    super();
  }

  initial() {
    this.controller = new AuthController();
    super.initial();
  }

  registerRoute() {
    this.router.post('/register', validate(authValidation.register()), this.responseHandler(this.controller.register))
    this.router.post('/login', validate(authValidation.login()), this.responseHandler(this.controller.login));
    this.router.post('/logout', validate(authValidation.logout()), this.responseHandler(this.controller.logout));
    this.router.post('/refresh-tokens', validate(authValidation.refreshTokens()), this.responseHandler(this.controller.refreshTokens));
  }
}

module.exports = AuthRoute
