const { RoomController } = require('../../controllers')
const RouteBase = require('../../base/route.base')
const { auth, validate } = require('../../middlewares');
const { roomValidation } = require('../../validations');

class RoomRoute extends RouteBase {

  constructor() {
    super();
  }

  initial() {
    this.controller = new RoomController();
    super.initial();
  }

  registerRoute() {
    // 建立房間
    this.router.post('/', auth(), validate(roomValidation.createRoom()), this.responseHandler(this.controller.createRoom));
    // 取得圖表類型
    this.router.get('/chart-type', this.responseHandler(this.controller.getChartType));
    // 取得房間投票資訊(用戶投了哪些選項)
    this.router.get('/:roomId/votes', this.responseHandler(this.controller.getRoomVotes));
    // 取得房間資訊
    this.router.get('/:roomId', this.responseHandler(this.controller.getRoom));
  }
}

module.exports = RoomRoute
