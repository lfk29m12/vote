const RouteBase = require('../base/route.base')
const UserRoute = require('./apis/user.route')
const AuthRoute = require('./apis/auth.route')
const RoomRoute = require('./apis/room.route')

class ApiRoute extends RouteBase {

  constructor() {
    super();
  }

  initial() {
    this.userRoute = new UserRoute()
    this.authRoute = new AuthRoute()
    this.roomRoute = new RoomRoute()
    super.initial();
  }

  registerRoute() {
    this.router.use('/user', this.userRoute.router);
    this.router.use('/auth', this.authRoute.router);
    this.router.use('/room', this.roomRoute.router);
  }

}

module.exports = ApiRoute
