const app = require('../../app')
const supertest = require('supertest')
const httpStatus = require('http-status');
const httpMocks = require('node-mocks-http');
const { auth } = require('../../middlewares')
const setupTestDB = require('../utils/setupTestDB')
const ApiError = require('../../response/apiError')
const { userFactory } = require('../../factories')
const config = require('../../config/config')
const { TokenService } = require('../../services')
const { TOKEN_TYPE_ACCESS, TOKEN_TYPE_REFRESH } = require('../../config/token');
const dayjs = require('dayjs');
const { roleRights } = require('../../config/roles')

const { insertUsers, createUser1Data, createUser2Data } = userFactory

setupTestDB()

const createUser1 = async () => {
  const result = await insertUsers({
    data: [createUser1Data()],
    withToken: true
  })
  return result[0]
}

describe('Auth Api', () => {
  describe('POST /api/auth/register', () => {
    test('成功註冊', async () => {
      const params = {
        email: 'lfk29m10@gmail.com',
        password: '1234',
        name: 'tester10'
      }

      const res = await supertest(app)
        .post("/api/auth/register")
        .send(params)
        .expect(httpStatus.CREATED)
    });

    test('註冊失敗 - body缺少參數', async () => {
      const params = {
        email: 'lfk29m10@gmail.com',
        password: '1234',
      }

      // App is used with supertest to simulate server request
      const response = await supertest(app)
        .post("/api/auth/register")
        .send(params)
        .expect(httpStatus.BAD_REQUEST)
    });
  });

  describe('POST /api/auth/login', () => {
    test('成功登入', async () => {
      await insertUsers({
        data: [createUser1Data()],
        withToken: true
      })

      const params = {
        email: 'tester1@gmail.com',
        password: '1234',
      }

      // App is used with supertest to simulate server request
      const response = await supertest(app)
        .post("/api/auth/login")
        .send(params)
        .expect(httpStatus.OK)
    });

  });
});

describe('Auth middleware', () => {
  test('should call next with no errors if access token is valid', async () => {
    const { user, token } = await createUser1()
    const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${token}` } });
    const next = jest.fn();

    await auth()(req, httpMocks.createResponse(), next);

    expect(next).toHaveBeenCalledWith();
    expect(req.user.id).toEqual(user.id);
  });

  test('should call next with unauthorized error if access token is not found in header', async () => {
    const { user, token } = await createUser1()
    const req = httpMocks.createRequest();
    const next = jest.fn();

    await auth()(req, httpMocks.createResponse(), next);

    expect(next).toHaveBeenCalledWith(expect.any(ApiError));
    expect(next).toHaveBeenCalledWith(
      expect.objectContaining({ status: httpStatus.UNAUTHORIZED, message: '權限不足' })
    );
  });

  test('should call next with unauthorized error if access token is not a valid jwt token', async () => {
    const { user, token } = await createUser1()
    const req = httpMocks.createRequest({ headers: { Authorization: 'Bearer randomToken' } });
    const next = jest.fn();

    await auth()(req, httpMocks.createResponse(), next);

    expect(next).toHaveBeenCalledWith(expect.any(ApiError));
    expect(next).toHaveBeenCalledWith(
      expect.objectContaining({ status: httpStatus.UNAUTHORIZED, message: '權限不足' })
    );
  });

  test('should call next with unauthorized error if the token is not an access token', async () => {
    const { user, token } = await createUser1()
    const expires = dayjs().add(config.jwt.accessExpirationMinutes, 'minute');
    const refreshToken = new TokenService().generateToken(user.id, expires, TOKEN_TYPE_REFRESH);
    const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${refreshToken}` } });
    const next = jest.fn();

    await auth()(req, httpMocks.createResponse(), next);

    expect(next).toHaveBeenCalledWith(expect.any(ApiError));
    expect(next).toHaveBeenCalledWith(
      expect.objectContaining({ status: httpStatus.UNAUTHORIZED, message: '權限不足' })
    );
  });

  test('should call next with unauthorized error if access token is generated with an invalid secret', async () => {
    const { user, token } = await createUser1()
    const expires = dayjs().add(config.jwt.accessExpirationMinutes, 'minute');
    const accessToken = new TokenService().generateToken(user.id, expires, TOKEN_TYPE_ACCESS, 'invalidSecret');
    const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${accessToken}` } });
    const next = jest.fn();

    await auth()(req, httpMocks.createResponse(), next);

    expect(next).toHaveBeenCalledWith(expect.any(ApiError));
    expect(next).toHaveBeenCalledWith(
      expect.objectContaining({ status: httpStatus.UNAUTHORIZED, message: '權限不足' })
    );
  });

  test('should call next with unauthorized error if access token is expired', async () => {
    const { user, token } = await createUser1()
    const expires = dayjs().subtract(1, 'minute');
    const accessToken = new TokenService().generateToken(user.id, expires, TOKEN_TYPE_ACCESS);
    const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${accessToken}` } });
    const next = jest.fn();

    await auth()(req, httpMocks.createResponse(), next);

    expect(next).toHaveBeenCalledWith(expect.any(ApiError));
    expect(next).toHaveBeenCalledWith(
      expect.objectContaining({ status: httpStatus.UNAUTHORIZED, message: '權限不足' })
    );
  });

  test('should call next with unauthorized error if user is not found', async () => {
    const req = httpMocks.createRequest({ headers: { Authorization: `Bearer Fake` } });
    const next = jest.fn();

    await auth()(req, httpMocks.createResponse(), next);

    expect(next).toHaveBeenCalledWith(expect.any(ApiError));
    expect(next).toHaveBeenCalledWith(
      expect.objectContaining({ status: httpStatus.UNAUTHORIZED, message: '權限不足' })
    );
  });

})
