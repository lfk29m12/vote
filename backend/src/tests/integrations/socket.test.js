const setupTestDB = require('../utils/setupTestDB')
const setupSocket = require('../utils/setupSocket')
const SocketHelper = require('../../socket/helper');
const { userFactory } = require('../../factories')

const { createUser1Data, insertUsers } = userFactory

setupTestDB()

describe("Socket", () => {
  describe('Socket auth middleware', () => {
    test("client 的 header 有攜帶 JWT token", async () => {
      let socketUser
      const { startSetup, closeSocket } = setupSocket({
        arrange: async ({ createClients }) => {
          const result = await insertUsers({
            data: [createUser1Data()],
            withToken: true
          })
          const { user, token } = result[0]
          socketUser = user
          await createClients([token])
        }
      })

      const { io, clientSockets, serverSockets } = await startSetup()
      const serverSocket = serverSockets[0]
      const clientSocket = clientSockets[0]

      await new Promise((resolve) => {
        serverSocket.on("hi", (value) => {
          const socketHelper = new SocketHelper({ io, socket: serverSocket });
          const userId = socketHelper.getUserId();
          expect(userId).toBe(socketUser.id);
          expect(value).toBe('Hello');
          resolve();
        });
        clientSocket.emit("hi", 'Hello');
      })

      await closeSocket()
    })

    // test("client 的 header 沒攜帶 JWT token => 被 auth middleware 阻擋並回傳 Please authenticate", async () => {
    //   let errorMsg
    //   const setting = { io: null, serverSocket: null, clientSocket: null }
    //   const { startSetup, closeSocket } = setupSocket({
    //     setting,
    //     cb: async ({ createClients, resolve }) => {
    //       const result = await insertUsers({
    //         data: [createUser1Data()],
    //         withToken: true
    //       })
    //       const { user, token } = result[0]
    //       setting.clientSocket = createClients()
    //       setting.clientSocket.on("connect_error", (err) => {
    //         errorMsg = err.message
    //         resolve()
    //       });
    //     }
    //   })

    //   await startSetup()

    //   expect(errorMsg).toBe('Please authenticate')

    //   await closeSocket({
    //     clientSockets: [setting.clientSocket]
    //   })
    // })
  })

});
