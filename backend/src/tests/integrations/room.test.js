const app = require('../../app')
const supertest = require('supertest')
const httpStatus = require('http-status');
const setupTestDB = require('../utils/setupTestDB')
const setupSocket = require('../utils/setupSocket')
const { roomHandler } = require('../../socket/handlers')
const { userFactory, roomFactory, voteOptionFactory } = require('../../factories')
const { Room, User } = require('../../models')
const { JOIN_ROOM, UPDATE_TOTAL_USER_IN_ROOM } = require('../../config/socket');
const redis = require('../../redis')

const { insertUsers, createUser1Data, createUser2Data, createUser3Data } = userFactory
const { insertRoom, createRoom1Data } = roomFactory
const { createVoteOption1Data, createVoteOption2Data } = voteOptionFactory

setupTestDB()

describe('Room Api', () => {
  describe('GET /room/:roomId', () => {
    test('房主取得房間資訊', async () => {
      // Arrange
      const result = await insertUsers({
        data: [createUser1Data()],
        withToken: true
      })
      const { user: owner, token } = result[0]
      const room = await owner.createOwnRoom({
        ...createRoom1Data(),
        options: [
          createVoteOption1Data(),
          createVoteOption2Data(),
        ]
      }, {
        include: [
          {
            association: User.Room,
          },
          {
            association: Room.Options,
          },
        ]
      })

      // Act
      const res = await supertest(app)
        .get(`/room/${room.id}`)
        .set('Authorization', 'Bearer ' + token)
        .expect(httpStatus.OK)

      // Assert
      const { body } = res
      expect(body.id).toEqual(room.id)
      expect(body.owner.id).toEqual(owner.id)
      expect(body.owner.name).toEqual(owner.name)
      expect(body.is_voted).toEqual(false)
      expect(body.options.length).toEqual(2)
      // 判斷陣列中是否包含某一物件中的部分屬性
      expect(body.options).toContainEqual(
        expect.objectContaining(createVoteOption1Data()),
        expect.objectContaining(createVoteOption2Data()),
      )
    });

    test('其他成員取得房間資訊', async () => {
      // Arrange
      const result = await insertUsers({
        data: [createUser1Data(), createUser2Data()],
        withToken: true
      })
      const { user: owner, token: ownerToken } = result[0]
      const { user: user2, token: user2Token } = result[1]
      const roomData = createRoom1Data({ owner_id: owner.id })
      const room = await user2.createRoom(
        {
          ...roomData,
          options: [
            createVoteOption1Data(),
            createVoteOption2Data(),
          ]
        },
        {
          through: { is_voted: 1 },
          include: [
            {
              association: User.Room,
            },
            {
              association: Room.Options,
            },
          ]
        }
      )

      // Act
      const res = await supertest(app)
        .get(`/room/${room.id}`)
        .set('Authorization', 'Bearer ' + user2Token)

      // Assert
      const { status, body } = res
      expect(status).toEqual(httpStatus.OK)
      expect(body.id).toEqual(room.id)
      expect(body.owner.id).toEqual(owner.id)
      expect(body.is_voted).toEqual(true)
      expect(body.options.length).toEqual(2)
      // 判斷陣列中是否包含某一物件中的部分屬性
      expect(body.options).toContainEqual(
        expect.objectContaining(createVoteOption1Data()),
        expect.objectContaining(createVoteOption2Data()),
      )
    });

    test('房間不存在, 返回 404', async () => {
      const result = await insertUsers({
        data: [createUser1Data()],
        withToken: true
      })
      const { user: owner, token } = result[0]
      const room = await owner.createOwnRoom(createRoom1Data())

      const res = await supertest(app)
        .get(`/room/9999`)
        .set('Authorization', 'Bearer ' + token)
        .expect(httpStatus.NOT_FOUND)
    });
  });
});

describe('ROOM SOCKET', () => {
  describe('加入房間', () => {
    test('user1, user2 加入房間 => user1收到房間總人數: 2', async () => {
      // Arrange
      let room
      const { startSetup, closeSocket } = setupSocket({
        arrange: async ({ createClients }) => {
          const result = await insertUsers({
            data: [createUser1Data(), createUser2Data()],
            withToken: true
          })
          const { user: owner, token: ownerToken } = result[0]
          const { user: user2, token: user2Token } = result[1]
          const roomData = createRoom1Data({ owner_id: owner.id })
          room = await user2.createRoom(
            {
              ...roomData,
              options: [
                createVoteOption1Data(),
                createVoteOption2Data(),
              ]
            },
            {
              through: { is_voted: 1 },
              include: [
                {
                  association: User.Room,
                },
                {
                  association: Room.Options,
                },
              ]
            }
          )
          await createClients([ownerToken, user2Token])
        }
      })

      const { io, clientSockets, serverSockets } = await startSetup()
      const [ownerSocket, user2Socket] = clientSockets
      const [ownerServerSocket, userServerSocket2] = serverSockets
      const redisClient = await redis.createClient()

      await new Promise((resolve) => {
        const onJoinRoom = ({ io, socket, redisClient }) => async ({ roomId }) => {
          const handler = roomHandler.onJoinRoom({ io, socket, redisClient })
          await handler({ roomId })
        }
        const onUpdateTotalUserInRoom = (totalUser) => {
          if (totalUser == 2) {
            expect(totalUser).toBe(2)
            resolve();
          }
        }

        ownerServerSocket.on(JOIN_ROOM, onJoinRoom({ io, socket: ownerServerSocket, redisClient }));
        userServerSocket2.on(JOIN_ROOM, onJoinRoom({ io, socket: userServerSocket2, redisClient }));

        ownerSocket.on(UPDATE_TOTAL_USER_IN_ROOM, onUpdateTotalUserInRoom);

        ownerSocket.emit(JOIN_ROOM, { roomId: room.id });
        user2Socket.emit(JOIN_ROOM, { roomId: room.id });
      })

      await closeSocket()
    })
    test('user1, user2 加入房間 => user2收到房間總人數: 2', async () => {
      // Arrange
      let room
      const { startSetup, closeSocket } = setupSocket({
        arrange: async ({ createClients }) => {
          const result = await insertUsers({
            data: [createUser1Data(), createUser2Data()],
            withToken: true
          })
          const { user: owner, token: ownerToken } = result[0]
          const { user: user2, token: user2Token } = result[1]
          const roomData = createRoom1Data({ owner_id: owner.id })
          room = await user2.createRoom(
            {
              ...roomData,
              options: [
                createVoteOption1Data(),
                createVoteOption2Data(),
              ]
            },
            {
              through: { is_voted: 1 },
              include: [
                {
                  association: User.Room,
                },
                {
                  association: Room.Options,
                },
              ]
            }
          )
          await createClients([ownerToken, user2Token])
        }
      })

      const { io, clientSockets, serverSockets } = await startSetup()
      const [ownerSocket, user2Socket] = clientSockets
      const [ownerServerSocket, userServerSocket2] = serverSockets
      const redisClient = await redis.createClient()

      await new Promise((resolve) => {
        const onJoinRoom = ({ io, socket, redisClient }) => async ({ roomId }) => {
          const handler = roomHandler.onJoinRoom({ io, socket, redisClient })
          await handler({ roomId })
        }
        const onUpdateTotalUserInRoom = (totalUser) => {
          if (totalUser == 2) {
            expect(totalUser).toBe(2)
            resolve();
          }
        }

        ownerServerSocket.on(JOIN_ROOM, onJoinRoom({ io, socket: ownerServerSocket, redisClient }));
        userServerSocket2.on(JOIN_ROOM, onJoinRoom({ io, socket: userServerSocket2, redisClient }));

        user2Socket.on(UPDATE_TOTAL_USER_IN_ROOM, onUpdateTotalUserInRoom);

        ownerSocket.emit(JOIN_ROOM, { roomId: room.id });
        user2Socket.emit(JOIN_ROOM, { roomId: room.id });
      })

      await closeSocket()
    })

  })
})
