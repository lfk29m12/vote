const { Server } = require("socket.io");
const Client = require("socket.io-client");
const { auth } = require('../../middlewares')
const app = require('../../app')

const setupSocket = ({ arrange }) => {
  const serverSockets = []
  const clientSockets = []
  let io

  const startSetup = () => {
    return new Promise(async (resolve, reject) => {
      const server = app.listen(async () => {
        io = new Server(server);

        // 驗證auth
        const wrapSocketMiddleware = middleware => (socket, next) => middleware(socket.request, {}, next)
        io.use(wrapSocketMiddleware(auth()))

        // new Client 建立連線時會先呼叫這邊, 之後才呼叫 client.on('connect')
        io.on("connection", (socket) => {
          serverSockets.push(socket)
        });

        if (typeof arrange === 'function') {
          await arrange({
            createClients: async (tokens = []) => {
              for (let token of tokens) {
                await _createClient({ server, token })
              }
              resolve({
                io,
                clientSockets,
                serverSockets,
              })
            },
            resolve
          })
        }
      });
    })
  }

  /**
   * 建立 client socket
   * @param {Object} param0
   * @param {Object} param0.server
   * @param {String} param0.token
   * @returns {Object} clientSocket
   */
  const _createClient = ({ server, token }) => {
    return new Promise((resolve) => {
      const port = server.address().port;
      const client = new Client(`http://localhost:${port}`, {
        transportOptions: {
          polling: {
            extraHeaders: {
              'Authorization': `Bearer ${token}`,
            },
          },
        },
      });

      client.on("connect", () => {
        clientSockets.push(client)
        resolve(client)
      });
    })

  }

  const closeSocket = async () => {
    io.close();
    for (let clientSocket of clientSockets) {
      clientSocket.close();
    }
  }

  return {
    startSetup,
    closeSocket
  }

};

module.exports = setupSocket;
