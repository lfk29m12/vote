const db = require('../../models')
const truncate = require('./truncate')

const setupTestDB = () => {
  // Before any tests run, clear the DB and run migrations with Sequelize sync()
  beforeAll(async () => {
    // 用 yarn db:migrate:test 代替, 且migration:undo:all時才方便
    // await db.sequelize.sync({ force: true })
  })

  beforeEach(async () => {
    await truncate();
  });

  // After all tests have finished, close the DB connection
  afterAll(async () => {
    await db.sequelize.close()
  })
};

module.exports = setupTestDB;
