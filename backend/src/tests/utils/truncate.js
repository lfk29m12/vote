const map = require('lodash/map');
const db = require('../../models')

// 錯誤版本, 因為會檢查外鍵導致truncate失敗
// module.exports = async function truncate() {
//   return await Promise.all(
//     map(Object.keys(db), (key) => {
//       if (['sequelize', 'Sequelize'].includes(key)) return null;
//       return db[key].destroy({ where: {}, force: true });
//     })
//   );
// }


module.exports = async function truncate() {
  const t = await db.sequelize.transaction();
  const options = { raw: true, transaction: t }
  try {
    await db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', options)
    for (let key of Object.keys(db)) {
      if (!['sequelize', 'Sequelize'].includes(key)) {
        const { tableName } = db[key]
        await db.sequelize.query(`truncate table ${tableName}`, options)
      }
    }
    await db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', options)
    await t.commit();
  } catch (error) {
    await t.rollback();
  }
}

