const express = require('express');
const path = require('path');
const compression = require('compression');
const cors = require('cors');
const httpStatus = require('http-status');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const { jwtStrategy } = require('./config/passport');
const morgan = require('./config/morgan');
const config = require('./config/config');
const { basicDateFormat } = require('./config/date');
const dayjs = require('dayjs');
const AppRoute = require('./routes/app.route')
const {
  error: { errorConverter, errorHandler },
} = require('./middlewares');
const ApiError = require('./response/apiError')

const app = express();

/**
 * app.set('json replacer', replacer) 會影響 response 時的 JSON.stringify 結果, 可自訂替換的邏輯
 * https://github.com/expressjs/express/issues/2792#issuecomment-153562157
 */
app.set('json replacer', function (key, value) {
  let newValue = value;

  // 將date轉換成標準日期格式
  if (this[key] instanceof Date) {
    newValue = dayjs(value).format(basicDateFormat);
  }

  return newValue;
});

if (config.env !== 'test') {
  app.use(morgan.successHandler);
  app.use(morgan.errorHandler);
}

app.use(express.static(path.join(__dirname, '../public')));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// parse cookie
app.use(cookieParser());

// gzip compression
app.use(compression());

// enable cors
app.use(cors());
app.options('*', cors());

// jwt authentication
app.use(passport.initialize());
// 透過 passport.use() 設定驗證請求策略
passport.use('jwt', jwtStrategy);

// api routes
const route = new AppRoute();
app.use('/', route.getRouter());

// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, '找不到Api'))
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

module.exports = app;
