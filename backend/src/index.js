const app = require('./app');
const config = require('./config/config');
const logger = require('./config/logger');
const socket = require('./socket');
const redis = require('./redis');
const { RoomService } = require('./services')
const auth = require('./middlewares/auth')
const { UnAuthorizedError } = require('./response/errors/base')
const { errorConverterKernel, errorHandlerKernel } = require('./middlewares/error')

async function init() {
  const server = app.listen(config.port, () => {
    logger.info(`Listening to port ${config.port}`);
  });

  global.redisClient = await redis.createClient();
  global.io = await socket.init(server);

  // socket middleware
  const wrapSocketMiddleware = middleware => (socket, next) => middleware(socket.request, {}, next)
  global.io.use(
    wrapSocketMiddleware(
      auth({
        onError: ({ req, res, next }) => {
          const err = new UnAuthorizedError()
          const error = errorHandlerKernel(errorConverterKernel(err))
          // 必須將錯誤資訊帶在data屬性上, 才能帶給client使用
          error.data = JSON.parse(JSON.stringify(error))
          next(error)
        }
      })
    )
  )


  // 主機重啟時, 清空redis所有房間的用戶列表
  await new RoomService().initRoomUsers(global.redisClient)

  const exitHandler = () => {
    if (server) {
      server.close(() => {
        logger.info('Server closed');
        process.exit(1);
      });
    } else {
      process.exit(1);
    }
  };

  const unexpectedErrorHandler = (error) => {
    logger.error(error);
    exitHandler();
  };

  process.on('uncaughtException', unexpectedErrorHandler);
  process.on('unhandledRejection', unexpectedErrorHandler);

  process.on('SIGTERM', () => {
    logger.info('SIGTERM received');
    if (server) {
      server.close();
    }
  });
}

init()


