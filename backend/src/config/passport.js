const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const config = require('./config');
const { TOKEN_TYPE_ACCESS } = require('./token');
const { UserRepository } = require('../repositories')

const userRepository = new UserRepository()

const jwtOptions = {
  secretOrKey: config.jwt.secret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), // 設定從 req 的哪裡取得jwt
};

/**
 *
 * @param {*} payload
 * @param {*} done - callback function，當驗證完成後，可以呼叫 done 並將驗證結果作為參數帶入，提供給 Passport 使用,
 *  done接收三個參數：
 *  1. 錯誤訊息：例如在伺服器端回傳錯誤訊息時，帶入錯誤訊息 err；無錯誤訊息時，則可以帶入null取代
 *  2. 使用者資料：驗證成功時，帶入使用者資料user；驗證失敗時，則可以帶入false取代
 *  3. 驗證失敗訊息：當驗證失敗時，可以額外補充驗證失敗的原因和資訊
 * @returns
 */
const jwtVerify = async (payload, done) => {
  try {
    // 只要沒有 access 權限就擋
    if (payload.type !== TOKEN_TYPE_ACCESS) {
      throw new Error('Invalid token type');
    }
    const user = await userRepository.getUserById(payload.sub);
    if (!user) {
      return done(null, false);
    }
    done(null, user);
  } catch (error) {
    done(error, false);
  }
};

// 參考: http://www.passportjs.org/packages/passport-jwt/
const jwtStrategy = new JwtStrategy(jwtOptions, jwtVerify);

module.exports = {
  jwtStrategy,
};
