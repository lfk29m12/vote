require('dotenv').config();

// 給 sequelize-cli 使用
module.exports = {
  development: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_NAME,
    host: process.env.SEQUELIZE_CLI_DB_HOST,
    dialect: process.env.DB_ENGINE,
  },
  test: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_TEST_NAME,
    host: process.env.SEQUELIZE_CLI_DB_HOST,
    dialect: process.env.DB_ENGINE,
  },
  // production: {
  //   username: 'root',
  //   password: null,
  //   database: 'database_production',
  //   host: '127.0.0.1',
  //   dialect: 'mysql',
  // },
};
