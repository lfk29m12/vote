const JOIN_ROOM = 'join_room';
const JOIN_ROOM_ERROR = 'join_room_error';
const LEAVE_ROOM = 'leave_room';
const UPDATE_TOTAL_USER_IN_ROOM = 'update_total_user_in_room';

module.exports = { JOIN_ROOM, JOIN_ROOM_ERROR, LEAVE_ROOM, UPDATE_TOTAL_USER_IN_ROOM };
