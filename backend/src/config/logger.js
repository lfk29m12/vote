const { format, createLogger, transports } = require('winston');

const { combine, splat, timestamp, printf, prettyPrint, colorize } = format;
const config = require('./config');

const enumerateErrorFormat = format((info) => {
  if (info instanceof Error) {
    Object.assign(info, { message: info.stack });
  }
  return info;
});

const logger = createLogger({
  level: config.env === 'development' ? 'debug' : 'info',
  format: combine(
    enumerateErrorFormat(),
    colorize(),
    splat(), // 功能如下: logger.log('info', 'test %d', 123); => 'test 123',
    timestamp(),
    prettyPrint(),
    printf(({ level, message }) => `${level}: ${message}`)
  ),
  transports: [new transports.Console()],
});

module.exports = logger;
