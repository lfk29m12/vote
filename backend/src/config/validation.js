// https://github.com/hapijs/joi/blob/master/API.md#list-of-errors
module.exports = {
  'any.required': '欄位必填',
  'string.empty': '字串不能為空',
  'date.greater': `日期必須大於 {#limit}`,
};
