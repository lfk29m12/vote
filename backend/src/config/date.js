const basicDateFormat = 'YYYY-MM-DD HH:mm:ss';

module.exports = {
  basicDateFormat,
};
