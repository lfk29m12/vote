const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const { INTEGER, STRING, BOOLEAN, DATE } = DataTypes;
  class UserVoteOptionPivot extends Model {
    static associate(models) {}
  }
  UserVoteOptionPivot.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: INTEGER,
      },
      reason: {
        type: STRING,
        allowNull: true,
      },
      user_id: {
        type: INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      vote_option_id: {
        type: INTEGER,
        references: {
          model: 'vote_options',
          key: 'id',
        },
      },
    },
    {
      sequelize,
      modelName: 'UserVoteOptionPivot',
      tableName: 'user_vote_option_pivots',
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );
  return UserVoteOptionPivot;
};
