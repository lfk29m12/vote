const { Model } = require('sequelize');
const bcrypt = require('bcryptjs');
const { roles } = require('../config/roles');

module.exports = (sequelize, DataTypes) => {
  const { INTEGER, STRING, BOOLEAN, DATE } = DataTypes;
  class User extends Model {
    static associate(models) {
      User.hasMany(models.Token, { sourceKey: 'id', foreignKey: 'user_id' });
      User.hasMany(models.Room, { as: 'ownRooms', sourceKey: 'id', foreignKey: 'owner_id' });
      User.Room = User.belongsToMany(models.Room, { through: models.UserRoomPivot, foreignKey: 'user_id', otherKey: 'room_id' });
      User.belongsToMany(models.VoteOption, { through: models.UserVoteOptionPivot, foreignKey: 'user_id', otherKey: 'vote_option_id', as: 'voters' });
    }

    /**
     * 比對密碼
     * @param {String} password
     * @returns {Boolean}
     */
    async isPasswordMatch(password) {
      return bcrypt.compare(password, this.password);
    }
  }

  User.init(
    {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: STRING,
        allowNull: false,
      },
      email: {
        type: STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: STRING,
        allowNull: false,
        // 因為 get/set 只能是同步的, 所以加密改成用 beforeCreate hook 處理
        // async set(password) {
        //   const hashed = await bcrypt.hash(password, 8);
        //   this.setDataValue('password', hashed);
        // },
      },
      is_email_verified: {
        type: BOOLEAN,
        defaultValue: 0,
      },
      role: {
        type: STRING,
        defaultValue: 'user',
        validate: {
          isIn: [roles],
        },
      },
      register_by: {
        type: INTEGER,
      },
    },
    {
      sequelize,
      modelName: 'User',
      tableName: 'users',
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      hooks: {
        async beforeCreate(user, options) {
          const hashed = await bcrypt.hash(user.password, 8);
          // eslint-disable-next-line no-param-reassign
          user.password = hashed;
        },
      },
      defaultScope: {
        attributes: {
          exclude: ['password'],
        },
      },
      scopes: {
        withPassword: {
          attributes: {
            include: ['password'],
          },
        },
      },
    }
  );

  return User;
};
