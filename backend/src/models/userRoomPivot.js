const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const { INTEGER, STRING, BOOLEAN, DATE } = DataTypes;
  class UserRoomPivot extends Model {
    static associate(models) {}
  }
  UserRoomPivot.init(
    {
      id: {
        type: INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      room_id: {
        type: INTEGER,
        references: {
          model: 'rooms',
          key: 'id',
        },
      },
      user_id: {
        type: INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      is_voted: {
        type: BOOLEAN,
        defaultValue: false,
      },
    },
    {
      sequelize,
      modelName: 'UserRoomPivot',
      tableName: 'user_room_pivots',
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );
  return UserRoomPivot;
};
