const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const { INTEGER, STRING, DATE, BOOLEAN } = DataTypes;
  class Token extends Model {
    static associate(models) {
      Token.belongsTo(models.User, { targetKey: 'id', foreignKey: 'user_id' });
    }
  }

  Token.init(
    {
      id: {
        type: INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      token: {
        type: STRING,
        allowNull: false,
      },
      user_id: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'users',
          },
          key: 'id',
        },
        onDelete: 'CASCADE',
      },
      type: {
        type: INTEGER(2).UNSIGNED,
        allowNull: false,
        comment: 'Token類別 (0: access, 1: refresh)',
      },
      expires: {
        type: DATE,
        allowNull: false,
      },
      blacklisted: {
        type: BOOLEAN,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'Token',
      tableName: 'tokens',
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return Token;
};
