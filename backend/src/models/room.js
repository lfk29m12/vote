const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const { INTEGER, STRING, DATE, BOOLEAN, UUID, UUIDV4 } = DataTypes;
  class Room extends Model {
    static associate(models) {
      Room.belongsTo(models.User, { as: 'owner', targetKey: 'id', foreignKey: 'owner_id' });
      Room.belongsToMany(models.User, { through: models.UserRoomPivot, foreignKey: 'room_id', otherKey: 'user_id' });
      Room.Options = Room.hasMany(models.VoteOption, { as: 'options', sourceKey: 'id', foreignKey: 'room_id' });
    }
  }
  Room.init(
    {
      id: {
        type: INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      uuid: {
        type: UUID,
        defaultValue: UUIDV4,
        allowNull: false,
      },
      owner_id: {
        type: INTEGER,
        references: {
          model: {
            tableName: 'users',
          },
          key: 'id',
        },
        onDelete: 'CASCADE',
      },
      title: {
        type: STRING,
        allowNull: false,
      },
      description: {
        type: STRING,
      },
      outdate_at: {
        type: DATE,
        allowNull: false,
      },
      chart_type: {
        type: INTEGER,
        defaultValue: 0,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'Room',
      tableName: 'rooms',
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return Room;
};
