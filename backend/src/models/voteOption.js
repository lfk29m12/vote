const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const { INTEGER, STRING, BOOLEAN, DATE } = DataTypes;
  class VoteOption extends Model {
    static associate(models) {
      VoteOption.belongsTo(models.Room, { targetKey: 'id', foreignKey: 'room_id', as: 'options' });
      VoteOption.belongsToMany(models.User, { through: models.UserVoteOptionPivot, foreignKey: 'vote_option_id', otherKey: 'user_id', as: 'voters' });
    }
  }
  VoteOption.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: INTEGER,
      },
      title: {
        type: STRING,
        require: true,
      },
      order: {
        type: INTEGER,
        defaultValue: 0,
        require: true,
      },
      room_id: {
        type: INTEGER,
        references: {
          model: 'rooms',
          key: 'id',
        },
      },
    },
    {
      sequelize,
      modelName: 'VoteOption',
      tableName: 'vote_options',
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );
  return VoteOption;
};
