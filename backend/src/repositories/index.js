module.exports.UserRepository = require('./user.repository')
module.exports.TokenRepository = require('./token.repository')
module.exports.RoomRepository = require('./room.repository')
module.exports.VoteOptionRepository = require('./voteOption.repository')
