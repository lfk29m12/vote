const { User } = require('../models')

class UserRepository {
  /**
   * @param {String} id
   * @param {Object} option
   * @returns
   */
  async getUserById(id, option = null) {
    const user = await User.findOne({
      where: { id },
      ...option,
    });
    return user
  }

  /**
   * @param {String} email
   * @param {String|Array} scopes
   * @returns {Object<User>}
   */
  async getUserByEmail(email, scopes = 'defaultScope') {
    return User.scope(scopes).findOne({
      where: {
        email,
      },
    });
  }

  /**
   * 抓取房間的User
   * @param {*} room
   * @param {*} userId
   * @param {*} option
   * @returns
   */
  async getRoomUsers(room, userId, option = null) {
    return await room.getUsers({
      where: { id: userId },
      ...option
    })
  }

  /**
   * 確認信箱是否已被使用
   * @param {String} email - 會員信箱
   * @returns {Promise<boolean>}
   */
  async isEmailTaken(email) {
    const user = await User.findOne({
      where: {
        email,
      },
    });
    return !!user;
  }

}

module.exports = UserRepository


