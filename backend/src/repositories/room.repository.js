const { Room } = require('../models')
const redis = require('../redis')

class RoomRepository {
  async getRoomById(id, option = null) {
    return await Room.findOne({
      where: { id },
      ...option,
    });
  }

  /**
   * 建立redis專用的房間key
   * @param {String} id - room id
   * @param {String} namespace - 命名空間
   * @returns
   */
  createRedisRoomKey(id, namespace) {
    if (!namespace) return `rooms:${id}`
    return `rooms:${id}:${namespace}`;
  }

  // ============
  // redis 操作
  // ============
  async redisAddClientToRoom({ redisClient, redisRoomKey, userIds, option }) {
    return await redisClient.SADD(redisRoomKey, userIds, option)
  }

  async redisRemoveClientFromRoom({ redisClient, redisRoomKey, userIds }) {
    return await redisClient.SREM(redisRoomKey, userIds);
  }

  /**
   * 計算該房間線上人數
   * @param {*} param0
   * @param {RedisClient} param0.redisClient
   * @param {String} param0.redisRoomKey
   * @returns {Number} 房間總人數
   */
  async redisCountRoomUsers({ redisClient, redisRoomKey }) {
    return await redisClient.SCARD(redisRoomKey)
  }

  /**
   * 取得所有房間的key
   * @param {Object} param0
   * @param {RedisClient} param0.redisClient
   * @param {String} param0.namespace
   * @returns {Array}
   */
  async redisGetRoomKeys({ redisClient, namespace }) {
    const redisRoomKeys = await redis.getKeys(redisClient, {
      pattern: this.createRedisRoomKey('*', namespace)
    })
    return redisRoomKeys
  }

  async redisClearAllRoom({ redisClient, redisRoomKeys }) {
    return await redisClient.DEL(redisRoomKeys)
  }

}

module.exports = RoomRepository
