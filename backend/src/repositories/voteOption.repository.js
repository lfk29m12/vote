const { VoteOption } = require('../models')

class VoteOptionRepository {
  async getVoteOptionByRoomId({ roomId, option }) {
    const voteOptions = await VoteOption.findAll({
      where: { room_id: roomId },
      ...option
    })
    return voteOptions
  }
}

module.exports = VoteOptionRepository
