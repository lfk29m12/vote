const { Token } = require('../models')

class TokenRepository {

  async findToken({ token, userId, type, blacklisted = false }) {
    return await Token.findOne({
      where: { token, type, user_id: userId, blacklisted },
    });
  }

  async findTokenWithoutUserId({ token, type, blacklisted = false }) {
    return await Token.findOne({
      where: { token, type, blacklisted },
    });
  }

  async createToken({ token, userId, expires, type, blacklisted }) {
    const tokenModel = await Token.create({
      token,
      user_id: userId,
      expires: expires.toDate(),
      type,
      blacklisted,
    });
    return tokenModel
  }

}

module.exports = TokenRepository


