const ControllerBase = require('../base/controller.base')
const { UserRepository } = require('../repositories')
const httpStatus = require('http-status')
const { UserService, TokenService, AuthService } = require('../services');
const { UserDTO } = require('../dtos')

class AuthController extends ControllerBase {
  constructor() {
    super()
    this.userRepo = new UserRepository()
    this.userService = new UserService()
    this.tokenService = new TokenService()
    this.authService = new AuthService()
  }

  async register(req, res) {
    const user = await this.userService.createUser(req.body);
    const tokens = await this.tokenService.generateAuthTokens(user);
    return this.formatResponse({
      user: new UserDTO(user),
      tokens,
    }, httpStatus.CREATED);
  }

  async login(req, res) {
    const { email, password } = req.body;
    const user = await this.authService.loginUserWithEmailAndPassword(email, password);
    const tokens = await this.tokenService.generateAuthTokens(user);
    return this.formatResponse({
      user: new UserDTO(user),
      tokens,
    }, httpStatus.OK);
  }

  async logout(req, res) {
    await this.authService.logout(req.body.refreshToken);
    return this.formatResponse('', httpStatus.NO_CONTENT);
  }

  async refreshTokens(req, res) {
    const { refreshToken } = req.body;
    const tokens = await this.authService.refreshAuth(refreshToken);
    return this.formatResponse({ ...tokens }, httpStatus.OK);
  }

}

module.exports = AuthController
