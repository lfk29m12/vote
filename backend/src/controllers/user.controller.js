const ControllerBase = require('../base/controller.base')
const { UserRepository } = require('../repositories')
const httpStatus = require('http-status')
const { UserService } = require('../services');

class UserController extends ControllerBase {
  constructor() {
    super()
    this.userRepo = new UserRepository()
    this.userService = new UserService()
  }

  async getUser(req, res) {
    const user = await this.userService.getUser(req.params.userId)
    if (!user) {
      return this.formatResponse('User not found', httpStatus.NOT_FOUND);
    }
    return this.formatResponse(user, httpStatus.OK);
  }

  async index(req, res) {
    const result = await this.userService.index(req.query);
    return this.formatResponse(result, httpStatus.OK);
  }

  async readProfile(req, res) {
    const user = await this.userService.getUser(req.user.id)
    if (!user) {
      return this.formatResponse('User not found', httpStatus.NOT_FOUND);
    }
    return this.formatResponse(user, httpStatus.OK);
  }

}

module.exports = UserController
