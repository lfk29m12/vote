const httpStatus = require('http-status');
const ControllerBase = require('../base/controller.base')
const { RoomService } = require('../services');

class RoomController extends ControllerBase {
  constructor() {
    super()
    this.roomService = new RoomService()
  }

  async createRoom(req, res) {
    const room = await this.roomService.create(req.user, req.body);
    return this.formatResponse(room, httpStatus.CREATED)
  }

  async getChartType(req, res) {
    const chartTypes = await this.roomService.getChartType();
    return this.formatResponse(chartTypes, httpStatus.OK)
  }

  async getRoom(req, res) {
    const { user, params: { roomId } } = req
    const room = await this.roomService.getRoom({ roomId, userId: user.id });
    return this.formatResponse(room, httpStatus.OK)
  }

  async getRoomVotes(req, res) {
    const { params: { roomId } } = req
    const voteOptions = await roomService.getRoomVotes(roomId);
    return this.formatResponse(voteOptions, httpStatus.OK)
  }
}


module.exports = RoomController
